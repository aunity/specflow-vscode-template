- Install the specflow templates in .net core with the command 
    
    ``` c#
      dotnet new -i SpecFlow.Templates.DotNet
    ```

 -  To Debug you will have to first set  

    ```
      $env:VSTEST_HOST_DEBUG = 1 
    ```

    in your terminal then go to the attach debugger and select the process id it is attached to. 

 - if you want to filter the tests which you want to debug 
    
    ``` c#
      dotnet test --filter TestCategory = debug 
    ```
    (you will add tag as @debug for the test you are running)

 - To test the whole project you have to run dotnet test in    the terminal

 - vs code does not have a plugin to generate the stepdefinition from the features file so we have to run the test and then in the error panel it will tell you what steps to copy.

 There are few issues that it does not work if you do not have a specflow.json file at the room and that should have the following code in it 

 ```
 { "runtime": { "missingOrPendingStepsOutcome": "Error" } }
 ``` 

- Adding Living document to the project 

   - add a reference to Living Document and then run the command

    ``` c#
      livingdoc test-assembly C:\Users\FVaz\source\repos\SpecFlowProjectTemplate\bin\Debug\netcoreapp3. 1\SpecFlowProjectTemplate.dll -t  C:\Users\FVaz\source\repos\SpecFlowProjectTemplate\bin\Debug\netcoreapp3.1\TestExecution.json
    ```

    if you just want the features without the result on a webpage then you can run

    ``` c#
      livingdoc feature-folder C:\Users\FVaz\source\repos\SpecFlowProjectTemplate\Features
    ```


