Feature: Get quotes
	As a prospective customer  I want Get health quotes

@PHI
Scenario Outline: Get Quote Scenario
    Given I am on the Quotes page
    When  I Select my cover type as <coverType> 
    *  I Select my state as <state> 
    *  I Select my hosital cover as <hospitalCover> 
    *  I Select my extra s cover as <extrasCover> 
	*  I Submit the form
Then I should get some recommendations
Examples:
| coverType | state | hospitalCover | extrasCover |
| Single   | NSW   | Medium        | Medium      |
#| couples   | VIC   | Medium        | Low         |

