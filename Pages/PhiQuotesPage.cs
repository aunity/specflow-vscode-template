using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;

namespace SpecFlowProjectTemplate.Pages
{
    public class PhiQuotesPage
    {
        private readonly IWebDriver driver;

        public PhiQuotesPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public void SetScale(string scale)
        {
            driver.FindElement(By.XPath("/html/body/main/div/div/div/div/div/div/div/div/div/form/fieldset[1]/section[1]/div/div[1]")).Click();
            List<IWebElement> qstnsList = driver.FindElement(By.XPath("/html/body/main/div/div/div/div/div/div/div/div/div/form/fieldset[1]/section[1]/div/div[2]/div")).FindElements(By.ClassName("choices__item--selectable")).ToList();

            foreach (var divQstn in qstnsList)
            {
                if (divQstn.Text.Contains(scale))
                {
                    divQstn.Click();
                    System.Threading.Thread.Sleep(200);
                    return;
                }
            }
        }

        public void SetState(string state)
        {
            driver.FindElement(By.XPath("/html/body/main/div/div/div/div/div/div/div/div/div/form/fieldset[1]/section[2]/div/div[1]")).Click();
            List<IWebElement> qstnsList = driver.FindElement(By.XPath("/html/body/main/div/div/div/div/div/div/div/div/div/form/fieldset[1]/section[2]/div/div[2]/div")).FindElements(By.ClassName("choices__item--selectable")).ToList();

            foreach (var divQstn in qstnsList)
            {
                if (divQstn.Text.Contains(state))
                {
                    divQstn.Click();
                    System.Threading.Thread.Sleep(200);
                    return;
                }
            }
        }



        public void SetHospitalCover(string hospitalCover)
        {
            driver.FindElement(By.XPath("/html/body/main/div/div/div/div/div/div/div/div/div/form/fieldset[2]/section[1]/div/div[1]")).Click();
            List<IWebElement> qstnsList = driver.FindElement(By.XPath("/html/body/main/div/div/div/div/div/div/div/div/div/form/fieldset[2]/section[1]/div/div[2]")).FindElements(By.ClassName("choices__item--selectable")).ToList();
            foreach (var divQstn in qstnsList)
            {
                if (divQstn.Text.Contains(hospitalCover))
                {
                    divQstn.Click();
                    System.Threading.Thread.Sleep(200);
                    return;
                }
            }
        }
        public void SetExtrasCover(string extraCover)
        {
            driver.FindElement(By.XPath("/html/body/main/div/div/div/div/div/div/div/div/div/form/fieldset[2]/section[2]/div/div[1]")).Click();
            List<IWebElement> qstnsList = driver.FindElement(By.XPath("/html/body/main/div/div/div/div/div/div/div/div/div/form/fieldset[2]/section[2]/div/div[2]")).FindElements(By.ClassName("choices__item--selectable")).ToList();
            if (qstnsList.Count > 0)
            {
                foreach (var divQstn in qstnsList)
                {
                    if (divQstn.Text.Equals(extraCover))
                    {
                        divQstn.Click();
                        System.Threading.Thread.Sleep(200);
                        return;
                    }
                }
            }
        }

        public void ClickRecommendation()
        {
            driver.FindElement(By.Id("recommendationsBtn")).Click();
        }
    }

}