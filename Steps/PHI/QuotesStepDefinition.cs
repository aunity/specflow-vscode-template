using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using SpecFlowProjectTemplate.Pages;
using TechTalk.SpecFlow;

namespace SpecFlowProjectTemplate.Steps.PHI
{
    [Binding]
    public class QuotesStepDefinition{
        private readonly ScenarioContext scenarioContext;
        IWebDriver driver;
        PhiQuotesPage quotesPage;

        public QuotesStepDefinition(ScenarioContext scenarioContext)
        {
            this.scenarioContext = scenarioContext;
        }

        [Given(@"I am on the Quotes page")]
         public void GivenIAmOnTheQuotesPage()
         {
             driver = new ChromeDriver(@"C:\Users\FVaz\testing\delete\specflow-vscode-template\Driver",GetCapability());
             driver.Navigate().GoToUrl("https://www.australianunity.com.au/health-insurance/quote");
             quotesPage = new PhiQuotesPage(driver);   
         }  

         [When(@"I Select my cover type as (.*)")]
         public void WhenISelectMyCoveTypeAs (string coverType)
         {
             quotesPage.SetScale(coverType);
         }

        [When(@"I Select my state as (.*)")]
         public void WhenISelectMyStateAs(string state)
         {
             quotesPage.SetState(state);
         }

         [When(@"I Select my hosital cover as (.*)")]
         public void WhenISelectMyHositalCoverAs(string hospitalCover)
         {
             quotesPage.SetHospitalCover(hospitalCover);
         }

         [When(@"I Select my extra s cover as (.*)")]
         public void WhenISelectMyExtraSCoverAs(string extraCover)
         {
             quotesPage.SetExtrasCover(extraCover);
         }

         [When(@"I Submit the form")]
         public void WhenISubmitTheForm()
         {
             quotesPage.ClickRecommendation();
         }

         [Then(@"I should get some recommendations")]
         public void ThenIShouldGetSomeRecommendations()
         {
          
         }



        public ChromeOptions GetCapability()
        {
            ChromeOptions options = new ChromeOptions();
            options.AddArguments("--start-maximized");
            options.AddArguments("no-sandbox");
            options.AddArguments("--disable-extensions");
            options.AddArguments("--incognito");
            //options.AddArguments("--headless");
            options.AddArguments("--ignore-certificate-errors");
            options.AddArguments("javascriptEnable", "true");
            
            return options;
        }
    }
}